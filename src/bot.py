import hashlib
import redis
import time
from datetime import datetime

import secret
from reddit_base_bot import RedditBotBase
from scrapeVid import search_and_download_video
from stabVid import StabVid
import vidUpload
from helper import l
from helper import PicnicException

class OverblockingException(PicnicException):
    """
    This Exception is not due to technical issue, but because it can't be guaranteed, that the content is legal
    """
    pass

class Stabbot(RedditBotBase):

    def __init__(self, reddit_client_id,
                 reddit_client_secret,
                 reddit_user,
                 reddit_password,
                 pfycat_client_id,
                 pfycat_client_secret,
                 pfycat_username,
                 pfycat_password,
                 home_sub_name,
                 user_agent):
        super(Stabbot, self).__init__(reddit_client_id=reddit_client_id,
                                      reddit_client_secret=reddit_client_secret,
                                      reddit_user=reddit_user,
                                      reddit_password=reddit_password,
                                      home_sub_name=home_sub_name,
                                      user_agent=user_agent)
        self.stabilizer = StabVid()
        self.vidUploader = vidUpload.VidUpload(self.user_agent, self.debug, self.dry_run,
                                               pfycat_client_id,
                                               pfycat_client_secret,
                                               pfycat_username,
                                               pfycat_password)

        self.message_submission_name = "replies_from_stabbot"

        self.r = redis.Redis(
            host='redis',
            port=6379,
            password='')

    def handle_user_error(self, mention, text):
        """
        post on thread made by automod and ping summoner
        """
        text = "pinging /u/" + mention.author.name + "\n\n" + text
        if self.dry_run:
            l("message would be: " + text)
            return
        s = self.get_message_thread(self.assume_over_18(mention))
        l("replying...")
        s.reply(text)

    def censor(self, mention):
        # this function makes sure, stabbot doesn't process illegal content
        # if mention.subreddit_name_prefixed == 'r/stabbot':

        whitelist_sub = [self.home_sub, ]
        if mention.subreddit in whitelist_sub:
            return
        if mention.author in self.home_sub.contributor():
            return
        if mention.subreddit.subscribers < 5000:
            raise OverblockingException("Due to the small size of the sub-reddit, the sub's "
                                  "moderators aren't trusted to remove illegal content in a timely fashion.")
        if (datetime.now() - datetime.utcfromtimestamp(mention.submission.created)).seconds < 60*30\
                and mention.subreddit.subscribers < 100000:
            raise OverblockingException("This post is too new. It most be older to give moderators time to remove it, "
                                        "if it was illegal. Try again later.")

    def process_summon(self, mention):
        l("submission: " + mention.submission.id + " - " + mention.submission.shortlink)
        over_18 = self.assume_over_18(mention)
        start_time = time.time()
        input_path = search_and_download_video(mention.submission)
        cached_result = self.check_cache(input_path)
        if cached_result is None:
            self.censor(mention)
            l("stabbing...")
            self.stabilizer(input_path, "stabilized.mp4")
            proc_time = time.time() - start_time
            l("uploading...")
            uploaded_url = self.vidUploader('stabilized.mp4', over_18, mention)
            self.set_cache(uploaded_url, input_path)
            upload_time = time.time() - start_time - proc_time
        else:
            uploaded_url = cached_result
            proc_time = 0
            upload_time = 0

        return self.generate_text(uploaded_url, proc_time, upload_time, over_18, mention.submission.over_18,
                                  cached_result is not None)

    def generate_text(self, uploaded_url, proc_time, upload_time, assume_over_18, tag_over_18, cache_hit):
        if tag_over_18:
            nsfw_text = "# --- NSFW --- \n\n "
        elif assume_over_18:
            nsfw_text = "# --- possibly NSFW --- \n\n "
        else:
            nsfw_text = ""

        result_text = "\nI have stabilized the video for you: " + uploaded_url + "\n"

        if cache_hit:
            time_text = ""
        else:
            time_text = "\nIt took " + "%.f" % proc_time + " seconds to process " \
                        + "and " + "%.f" % upload_time + " seconds to upload.\n"

        foot_text = "^^[&nbsp;how&nbsp;to&nbsp;use]" \
                    "(https://www.reddit.com/r/stabbot/comments/72irce/how_to_use_stabbot/)" \
                    "&nbsp;|&nbsp;[programmer](https://www.reddit.com/message/compose/?to=wotanii)" \
                    "&nbsp;|&nbsp;[source&nbsp;code](https://gitlab.com/juergens/stabbot)" \
                    "&nbsp;|&nbsp;/r/ImageStabilization/" \
                    "&nbsp;|&nbsp;for&nbsp;cropped&nbsp;results,&nbsp;use&nbsp;\/u/stabbot_crop"

        return nsfw_text \
               + result_text \
               + time_text \
               + "___\n" \
               + foot_text

    def get_message_thread(self, over_18):
        query = "title:\"" + self.message_submission_name + "\""
        if over_18:
            query = query + " nsfw:yes"
        else:
            query = query + " nsfw:no"

        candidates = self.home_sub.search(query, sort="new")
        return next(candidates)  # should be the first element

    def assume_over_18(self, mention):
        if mention.submission.over_18:
            return True

        # if mention.subreddit_name_prefixed == 'r/stabbot':
        if mention.subreddit == self.home_sub:
            return False

        if mention.subreddit.subscribers < 500:
            return True

        return False

    def check_cache(self, input_path):
        input_md5 = hashlib.md5(open(input_path, 'rb').read()).hexdigest()
        return self.r.get("md5-" + input_md5)

    def set_cache(self, uploaded_url, input_path):
        input_md5 = hashlib.md5(open(input_path, 'rb').read()).hexdigest()
        self.r.set("md5-" + input_md5, uploaded_url)


# ####################### #
# ## excecution ######### #
# ####################### #

if __name__ == "__main__":
    b = Stabbot(reddit_client_id=secret.reddit_client_id,
                reddit_client_secret=secret.reddit_client_secret,
                reddit_user=secret.reddit_user,
                reddit_password=secret.reddit_password,
                pfycat_client_id=secret.pfycat_client_id,
                pfycat_client_secret=secret.pfycat_client_secret,
                pfycat_username=secret.pfycat_username,
                pfycat_password=secret.pfycat_password,
                home_sub_name="stabbot",
                user_agent="ubuntu:de.wotanii.stabbot:v0.6 (by /u/wotanii)")
    b()
