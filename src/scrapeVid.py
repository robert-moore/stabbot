import os
import youtube_dl
from helper import PicnicException


class VideoNotFoundException(PicnicException):
    pass


# ####################### #
# ## functions ########## #
# ####################### #

def search_and_download_video(submission):
    filename = u'download.mkv'
    ydl = youtube_dl.YoutubeDL({
        "outtmpl": filename,
        "quiet": True,
        "restrictfilenames": True})
    ydl.download([submission.url])
    for f in os.listdir("."):
        return f
    raise VideoNotFoundException("No Video found")
