import os
import uuid
import pfycat
import traceback
from helper import PicnicException
from helper import l

from pystreamable import StreamableApi

import peertube


class NoNSFWException(PicnicException):
    pass


class NoUploadException(PicnicException):
    pass


class VidUpload(object):

    def __init__(self, user_agent, debug, dryrun,
                 pfycat_client_id, pfycat_client_secret, pfycat_username, pfycat_password):
        import secret

        self.dryrun = dryrun
        self.debug = debug

        self.client_streamable = StreamableApi(secret.streamable_user, secret.streamable_pass)

        self.pfycatclient = pfycat.Client(client_id=pfycat_client_id, client_secret=pfycat_client_secret,
                                          user_name=pfycat_username, user_pass=pfycat_password)
        l(self.pfycatclient.me())

        self.peertube = peertube.uploader()

        self.upload_list = [
            # self.upload_file_streamable, #broken
            self.upload_file_peertube,
            self.upload_file_gfycat,
            # self.upload_file_insxnity, #broken
            # self.upload_file_openload, #hated by all
        ]

    def __call__(self, file_name, over_18, mention):
        return self.upload_file(file_name, over_18, mention)

    def upload_file_gfycat(self, locale_file_name, over_18, mention):
        r = self.pfycatclient.upload(locale_file_name)
        return "https://gfycat.com/" + r["gfyname"]

    def upload_file_peertube(self, locale_file_name, over_18, mention):

        descr = "stabilization for this video was first requested here: https://old.reddit.com" + mention.context
        descr = descr + "\n\nthis video was generated automagically by this tool: https://gitlab.com/juergens/stabbot"
        if over_18:
            privacy = 'unlisted'
            name = "NSFW - the greatest video"
        else:
            privacy = 'public'
            name = "the greatest video"

        r = self.peertube.upload(file=locale_file_name, name=name, nsfw=over_18, privacy=privacy,
                                 description=descr)
        return r

    def upload_file_streamable(self, locale_file_name, over_18, mention):
        if over_18:
            raise NoNSFWException()
        result = self.client_streamable.upload_video(locale_file_name, 'stable video')
        return 'https://streamable.com/' + result['shortcode']

    def upload_file(self, locale_file_name, over_18, mention):
        # need unique filename for openload
        oldext = os.path.splitext(locale_file_name)[1]
        newName = str(uuid.uuid4()) + oldext
        os.rename(locale_file_name, newName)

        if self.dryrun:
            return "https://streamable.com/swt6z"

        for f in self.upload_list:
            try:
                return f(newName, over_18, mention)
            except NoNSFWException:
                pass
            except Exception as e:
                print "Exception:"
                print e.__class__, e.__doc__, e.message
                print e
                traceback.print_exc()

        raise NoUploadException("could not upload stabilized file")
